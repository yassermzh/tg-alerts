#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'

conn = Bunny.new
conn.start

ch   = conn.create_channel
q    = ch.queue('tg_alerts', :durable => true)

msg  = ARGV.empty? ? 'Hello World!' : ARGV.join(' ')

10000.times do |n|
  #puts "#{msg}-#{n}"
  q.publish("#{msg}-#{n}", :persistent => true)
  puts " [x] Sent #{msg}-#{n}"
end

sleep 1.0
conn.close
