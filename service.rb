#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'
require_relative './telegram'
conn = Bunny.new
conn.start

ch   = conn.create_channel
q    = ch.queue('tg_alerts', :durable => true)

ch.prefetch(1)
puts ' [*] Waiting for tg alerts. To exit press CTRL+C'

tg = Telegram.new

begin
  q.subscribe(:manual_ack => true, :block => true) do |delivery_info, properties, body|
    puts " [x] Received '#{body}'"
    response = tg.send_message(body, '-1001104183304')
    #sleep body.count('.').to_i
    puts response
    if !response.nil?
      puts ' [x] Done'
      ch.ack(delivery_info.delivery_tag)
    end
  end
rescue Interrupt => _
  conn.close
end
