FROM ruby:2.3.1
RUN apt-get install libcurl4-openssl-dev
RUN mkdir -p /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . ./
CMD 'ruby service.rb'
