#require 'yaml'
#require 'uri'
require 'curb'
require 'json'

class Telegram

  class UnprocessableError < RuntimeError; end

  def initialize
    #config = YAML.load_file(File.join(__dir__, 'config/telegram.yml'))
    @uri = 'https://api.telegram.org/bot390693597:AAHSb9D0p7gpnVbz7Lk3gCWkAGDkeGgFFCc/sendMessage'
  end

  def send_message(msg, channel)
    q = { text: msg, chat_id: channel, parse_mode: 'Markdown' }
    #command = [@uri, q.to_query].join('?')
    command = "#{@uri}?text=#{msg}&chat_id=#{channel}"
    puts command
    res = Curl.get(command)
    begin
      check_response res
    rescue Exception => e
      puts("failed to send #{e.message}")
      return false
    end
    JSON.parse(res.body)['ok']
  end

  private

  def check_response(response)
    unless (200..299).member?(response.response_code)
      raise UnprocessableError, "Reponse with Error: #{response.body}"
    end

    unless response.content_type.eql?('application/json')
      raise UnprocessableError, "Reponse with bad content: '#{response.content_type}'"
    end
  end

end
